import threading
import time
import queue
import numpy as np
import os
import cv2 as cv
import unittest

_FRAMES = 100  # liczba ramek do obsłużenia
_WIDTH = 1024  # szerokość obrazu
_HEIGH = 768   # wysokość obrazu
_CHANNELS = 3  # liczba kanałów
_GENERATE_TIME = 0.05 # co ile sekund ma być wygenerowana ramka

class Source:
    def __init__(self, source_shape: tuple):
        self._source_shape: tuple = source_shape
    def get_data(self) -> np.ndarray:
        rows, cols, channels = self._source_shape
        return np.random.randint(
            256,
            size=rows * cols * channels,
            dtype=np.uint8,
        ).reshape(self._source_shape)

class ImageProcessor:
    def __init__(self, source_image: np.ndarray):
        self._source_image: np.ndarray = source_image
        self.__pocessed_image: np.ndarray = source_image
    def reduce2times(self) -> np.ndarray:
        # resize image
        (rows, columns, channels) = self.__pocessed_image.shape
        new_heigh = rows//2
        new_width = columns//2
        self.__pocessed_image = cv.resize(self.__pocessed_image, (new_width,new_heigh), interpolation = cv.INTER_AREA)
        return self.__pocessed_image
    def filter_median(self, size) -> np.ndarray:
        #median
        self.__pocessed_image = cv.medianBlur(self.__pocessed_image, size)
        return self.__pocessed_image
    def get_processed_image(self) -> np.ndarray:
        return self.__pocessed_image


class Producer(threading.Thread):
    def __init__(self, queue_a, frames):
        super().__init__()
        self.queue_a = queue_a
        self._frames = frames
        self._stop_event = threading.Event()
    # główna metoda wątku producenta
    def run(self):
        counter = 0
        while not self._stop_event.is_set():
            # Co 50 ms producent generuje obraz i wrzuca je do kolejki A
            time.sleep(_GENERATE_TIME)
            image_param = (_HEIGH,_WIDTH,_CHANNELS)
            image = Source(image_param)
            self.queue_a.put(image.get_data())
            if __debug__:
                print(f'Producent: wrzucono obraz do kolejki A ')
            counter += 1        
            # Jeśli funkcja została wywołana _FRAMES razy, zakończ działanie 
            if counter == self._frames:
                break
        self.stop()
       
    # zatrzymanie wątku - do testów
    def stop(self):
        self._stop_event.set()

class Consumer(threading.Thread):
    def __init__(self, queue_a, queue_b,frames):
        super().__init__()
        self.queue_a = queue_a
        self.queue_b = queue_b
        self._frames = frames
        self._stop_event = threading.Event()
    # główna metoda wątku konsumenta
    def run(self):
        counter = 0
        while not self._stop_event.is_set():
            # Ściągamy dostępne dane z kolejki A i dokonujemy następującej operacji przetwarzania
            image = self.queue_a.get()
            image_processor = ImageProcessor(image)
            image_processor.reduce2times()
            image_processor.filter_median(5)
            # Dodaj przetworzone dane do kolejki B
            self.queue_b.put(image_processor.get_processed_image())
            # Oznacz dane jako przetworzone
            self.queue_a.task_done()              
            counter += 1
            if __debug__:   
                print(f'Konsument: przekształcono obraz i wrzucono do kolejki B')
            # Po zliczeniu do _frames zapisujemy do plików kolejkę B
            if counter == self._frames:
                counter = 0
                self.stop()
                self.save_to_files()         
                
    # zapis do plików wszystkich elementów z kolejki B               
    def save_to_files(self):
        print(f'Zapis do plików danych z kolejki B')
        if not os.path.exists('processed'):
            os.makedirs('processed')        
        for index in range(self.queue_b.qsize()):
            # zapis za pomocą openCV
            cv.imwrite(f'processed/{index}.png', self.queue_b.get())
            self.queue_b.task_done()
    # zatrzymanie wątku 
    def stop(self):
        self._stop_event.set()


            

def main():
    print('Rozpoczęcie przetwarzania')
    # Stwórz kolejki
    queue_a = queue.Queue()
    queue_b = queue.Queue()

    # Stwórz obiekt producenta
    producer = Producer(queue_a,_FRAMES)
    # Stwórz obiekt konsumenta
    consumer = Consumer(queue_a, queue_b,_FRAMES)
    # Uruchom producenta
    producer.start()
    # Uruchom konsumenta
    consumer.start()
    # zaczekaj na zakończenie wątków
    producer.join()
    queue_a.join()
    queue_b.join()
    print('Zakończono przetwarzanie')
    

if __name__ == '__main__':
    main()





  