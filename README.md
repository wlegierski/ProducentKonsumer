# ProducentKonsumer

## Opis

Program realizuje w  architekturze Producent-Konsument dwie funkcje związane z przetwrzaniem obrazu:
- dwukrotne zmniejszenie rozmiaru obrazu,
- aplikuje filtr medianowy o kernelu 5x5.

Program składa się z 2 wątków:
- Producent - ma do dyspozycji źródło danych o rozmiarze
szerokość=1024 px, wysokość=768 px, liczba kanałów=3. Co 50 ms producent
pobiera nowe dane z Source i wrzuca je do kolejki A,
- Konsument - ściąga dostępne dane z kolejki A i dokonuje
dwóch  operacji przetwarzania. Po zakończeniu przetwarzania, nowy obraz wrzucony jest do kolejki B.

Po uruchomieniue programu następuje przetworzenie 100 ramek danych.
Następnie wszystkie dane z kolejki B są zapisane w katalogu o
nazwie processed w formacie png. 

## Realizacja

Program realizuje zadanie poprzez dwie klasy dziedziczące po threading.Thread:

- Klasa Producer generuje dane do kolejki queue_A wykorzystując zapronowaną klasę Source,
- Klasa Consumer pobiera elementu z kolejki queue_A i wkłada ją do kolejki queue_B. Obie kolejki są dla obu wątków zmiennymi globalnymi, dlatego mogą na nich operować, ale poprzez informowanie, że  kolejka queue_A zostałą już obsłużona poprzez metodę queue_a.task_done()  

Do realizacji użyto koncepcji globalnych (wspólnych dla obu wątków) obiektów  klasy Queue, a nie standardowej listy, ponieważ:
1. Queue zabezpiecza swój stan poporzez mutex (np. stanardowa  lista tego nie robi)
2. Queue umożliwia blokowanie wątku przy  get() (lista nie).
3. Queue umożliwia blokowanie wątku przy put() (lista nie).
Tym samym użycie Queue jest tzw. atomiczne oraz thread-safe.

Klasa ImageProcessor to klas posiadająca w sobie metody związane z przetarzaniem obrazów. Zachowuje ona oryginalny obraz, ale zakłada, że przetwarzanie jest typu pipeline, czyli realizowane jest na poprzednich wynikach przetwarzań. 

Na początku programu znajdują się stałe pozwalające na parametryzację działania programu:
```
_FRAMES = 100  # liczba ramek do obsłużenia
_WIDTH = 1024  # szerokość obrazu
_HEIGH = 768   # wysokość obrazu
_CHANNELS = 3  # liczba kanałów
_GENERATE_TIME = 0.05 # co ile sekund ma być wygenerowana ramka
```

W pliku test_ProducentKonsument.py znajdują się testy jednostkowe sprawdzające podstawowe działanie programu

W pliku ProducentKonsument_short.py jest natomiast alternatywna lakoniczna wersja programu. Ta wersja została dodana jako przykład realizacji, gdy nie zależy nam na czystym kodzie, a raczej na szybkości implementacji.

Aby uruchomić program należy użyć polecenia:
```
python .\ProducentKOnsument.py
```

W przypadku, gdy nie chcemy otrzymywać zbyt dużo komunikatów należy użyć wersji zopytymalizowanej: 
```
python -O .\ProducentKonsument.py
```
## Instalacja modułów zależnych

Oprogramowanie używa modułów numpy oraz cv2 (OpenCV). Gdyby nie były one zainstalowane należy użyć polecenia:
```
python .\install_requirements.py
```
