import queue
import unittest
import time
import numpy as np
from ProducentKonsument import Producer,Consumer,ImageProcessor,_FRAMES,_HEIGH,_WIDTH,_CHANNELS

class TestProducer(unittest.TestCase):
    def test_producer(self):
        # Stwórz kolejkę
        queue_a = queue.Queue()

        # Stwórz obiekt producenta
        producer = Producer(queue_a,_FRAMES)

        # Uruchom producenta w osobnym wątku
        producer.start()
        # Zatrzymaj producenta po 1 sekundzie
        time.sleep(1)
        producer.stop()

        # Sprawdź, czy w kolejce znajdują się jakieś dane
        self.assertFalse(queue_a.empty())
        self.assertEqual(queue_a.get().shape, (_HEIGH,_WIDTH,_CHANNELS))

class TestConsumer(unittest.TestCase):
    def test_consumer(self):
        # Stwórz kolejki
        queue_a = queue.Queue()
        queue_b = queue.Queue()

        # Stwórz obiekt producenta
        producer = Producer(queue_a,_FRAMES)
        # Stwórz obiekt konsumenta
        consumer = Consumer(queue_a, queue_b,_FRAMES)

         # Uruchom producenta w osobnym wątku
        producer.start()        

        # Uruchom konsumenta w osobnym wątku
        consumer.start()
        # Zatrzymaj producenta i konsumenta po 1 sekundzie
        time.sleep(1)
        producer.stop()
        consumer.stop()

        # Pobierz przetworzone dane z kolejki B
        result = queue_b.get()
        # Sprawdź, czy przetworzone dane są poprawne
        self.assertEqual(result.shape, (_HEIGH//2,_WIDTH//2,_CHANNELS))

class TestMedianFilter(unittest.TestCase):
    def test_median_filter(self):
    # Test 1: obraz o wymiarach 5x5    
        image = np.zeros((5, 5, 3), dtype=np.uint8)
        image[0, 0, 0] = 255
        image[0, 0, 1] = 255
        image[0, 0, 2] = 255
        image[0, 1, 0] = 255
        image[0, 1, 1] = 255
        image[0, 1, 2] = 255
        
        
        expected = np.zeros((5, 5, 3), dtype=np.uint8)
        expected[0, 0, 0] = 255
        expected[0, 0, 1] = 255
        expected[0, 0, 2] = 255

        result = ImageProcessor(image)
        image_processor = ImageProcessor(image)
        image_after_median = image_processor.filter_median(3)
        self.assertTrue(np.array_equal(image_after_median, expected))
    

if __name__ == '__main__':
    unittest.main()